package cern.dash.pjs;

import cern.dash.pjs.jpa.Element;
import cern.dash.pjs.jpa.ElementRepository;
import cern.dash.pjs.process.ElementEntityProcessor;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * Created by bcopy on 02/03/17.
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class ElementEntityProcessorIT {

    @Autowired
    ElementRepository elementRepository;

    @Autowired
    ElementEntityProcessor processor;

    @Test
    public void testEnrichProcess(){
//        Element element = new Element();
//
//        element.setId("my-element");
//        element.setUri("classpath:test-components/element.html");
//
//        elementRepository.save(element);


        processor.enrichElements();

    }
}
