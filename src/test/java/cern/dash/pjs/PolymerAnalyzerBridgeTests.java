package cern.dash.pjs;

import cern.dash.pjs.polymer.dto.ElementDto;
import cern.dash.pjs.process.PolymerAnalyzerJ2V8Delegator;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.SneakyThrows;
import org.junit.Test;
import org.springframework.core.io.ClassPathResource;

import java.util.concurrent.TimeUnit;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.fail;

/**
 * Created by bcopy on 02/03/17.
 */
public class PolymerAnalyzerBridgeTests {


    @Test
    @SneakyThrows
    public void simpleElementTest() {
      PolymerAnalyzerJ2V8Delegator pab = new PolymerAnalyzerJ2V8Delegator();
      ObjectMapper mapper = new ObjectMapper();
      String json = pab.getPolymerElementDescriptor(new ClassPathResource("test-components/element.html")).get(2, TimeUnit.SECONDS);
      JsonNode root = mapper.readTree(json);
      for(JsonNode attr : root.path("attributes")){
          assertEquals("who",attr.path("name").textValue());
      }
    }

    @Test
    @SneakyThrows
    public void polymerAnalyzerDtoDeserializeTest(){
        PolymerAnalyzerJ2V8Delegator pab = new PolymerAnalyzerJ2V8Delegator();
        ObjectMapper mapper = new ObjectMapper();
        String json = pab.getPolymerElementDescriptor(new ClassPathResource("test-components/element.html")).get(2, TimeUnit.SECONDS);

        ElementDto[] elements =mapper.readValue(json,ElementDto[].class);

        assertEquals("hello-world", elements[0].getTagName());
        assertEquals("who", elements[0].getAttributes().get(0).getName());
        assertEquals("who", elements[0].getProperties().get(0).getName());
        assertEquals("\"World\"", elements[0].getProperties().get(0).getDefaultValue());
    }


}
