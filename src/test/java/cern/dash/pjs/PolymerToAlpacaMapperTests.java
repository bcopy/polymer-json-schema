package cern.dash.pjs;

import cern.dash.pjs.alpacaschema.dto.SchemaDto;
import cern.dash.pjs.polymer.dto.ElementDto;
import cern.dash.pjs.process.PolymerAnalyzerJ2V8Delegator;
import cern.dash.pjs.process.PolymerToAlpacaMapper;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.SneakyThrows;
import org.junit.Test;
import org.springframework.core.io.ClassPathResource;

import java.util.concurrent.TimeUnit;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

/**
 * Created by bcopy on 02/03/17.
 */
public class PolymerToAlpacaMapperTests {
    @Test
    @SneakyThrows
    public void testSimpleMapping(){
        PolymerAnalyzerJ2V8Delegator pab = new PolymerAnalyzerJ2V8Delegator();
        ObjectMapper mapper = new ObjectMapper();
        String json = pab.getPolymerElementDescriptor(new ClassPathResource("test-components/element.html")).get(2, TimeUnit.SECONDS);

        assertNotNull(json);

        ElementDto element = mapper.readValue(json, ElementDto[].class)[0];

        assertNotNull(element);

        PolymerToAlpacaMapper pam = new PolymerToAlpacaMapper();
        SchemaDto schema = pam.convertPolymerElement(element);

        assertEquals(element.getTagName(), schema.getTitle());
        assertEquals(element.getProperties().size(), schema.getProperties().size());
    }
}
