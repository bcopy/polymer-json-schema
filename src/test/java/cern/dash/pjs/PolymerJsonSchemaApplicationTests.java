package cern.dash.pjs;

import cern.dash.pjs.config.PolymerCatalogConfig;
import cern.dash.pjs.jpa.*;
import cern.dash.pjs.jpa.Catalog;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@SpringBootTest
public class PolymerJsonSchemaApplicationTests {

	@Autowired
	private CatalogRepository repository;


	@Test
	public void contextLoads() {

		Catalog c = repository.findAll().iterator().next();
		assertNotNull(c);
		assertEquals(2, c.getElements().size());

	}

}
