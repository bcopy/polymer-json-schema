const Analyzer = require('polymer-analyzer').Analyzer;
const FSUrlLoader = require('polymer-analyzer/lib/url-loader/fs-url-loader').FSUrlLoader;
const PackageUrlResolver = require('polymer-analyzer/lib/url-loader/package-url-resolver').PackageUrlResolver;
const Promise = require('promise');
const stringifySafe = require('json-stringify-safe');

var __analyzer = new Analyzer({
    urlLoader: new FSUrlLoader("/"),
    urlResolver: new PackageUrlResolver(),
});


module.exports = {
    /**
     * Returns a Promise that produces a JSON document out of the Polymer Analyzer extraction
     * for the given file.
     *
     * @param elementFile
     * @returns Promise with the JSON dump result
     */
    extractJson: function (elementFile)
    {
        return new Promise(function (fulfill, reject) {
            __analyzer.analyze(elementFile)
                .then(function (document) {
                    try {
                        fulfill(stringifySafe(Array.from(document.getByKind('element', {imported: false}))));
                    } catch (ex) {
                        reject(ex);
                    }
                })
                .catch(function (error) {
                    reject(error);
                });
        });

    }

};