package cern.dash.pjs.alpacaschema.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by bcopy on 02/03/17.
 */
@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class SchemaDto {
    String title;

    String type = "object";

    Map<String,PropertyDto> properties = new HashMap<>();

}
