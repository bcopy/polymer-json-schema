package cern.dash.pjs.alpacaschema.dto;

import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * Created by bcopy on 02/03/17.
 */
@Data
@AllArgsConstructor

public class SchemaWrapperDto {
    SchemaDto schema;
    OptionsDto options = new OptionsDto();

    public SchemaWrapperDto(SchemaDto schemaDto){
        schema = schemaDto;
    }

}
