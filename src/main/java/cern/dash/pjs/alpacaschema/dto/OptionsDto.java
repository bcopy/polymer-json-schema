package cern.dash.pjs.alpacaschema.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by bcopy on 02/03/17.
 */
@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class OptionsDto {
    String helper;
    Map<String, FieldDto> fields = new HashMap<>();
}
