package cern.dash.pjs.alpacaschema.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by bcopy on 02/03/17.
 */
@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class FieldDto {
    Integer size;
    String helper;
    String placeholder;
    String type;
    Integer rows;
    Integer cols;
    List<String> optionLabels = new ArrayList<>();

}
