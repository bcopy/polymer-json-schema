package cern.dash.pjs.alpacaschema.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by bcopy on 02/03/17.
 */
@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class PropertyDto {
    String type;
    String title;
    @JsonProperty("enum")
    List<String> enumValues = new ArrayList<>();
}
