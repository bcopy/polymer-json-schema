package cern.dash.pjs;

import cern.dash.pjs.jpa.Catalog;
import cern.dash.pjs.jpa.CatalogRepository;
import cern.dash.pjs.jpa.Element;
import cern.dash.pjs.jpa.ElementRepository;
import cern.dash.pjs.process.ElementEntityProcessor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;

import javax.annotation.PostConstruct;
import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

/**
 * Initializes data for quick development and testing
 * Created by bcopy on 28/02/17.
 */
@Component
public class DataInitializer implements ApplicationRunner {
    @Autowired
    private CatalogRepository repository;

    @Autowired
    private ElementRepository elementRepository;

    @Autowired
    ElementEntityProcessor elementEntityProcessor;

    public DataInitializer(){}

    @Override
    public void run(ApplicationArguments args) throws Exception {
        Element dsBroadcastElement = new Element("ds-broadcast-4.0-SNAPSHOT","ds-broadcast");
//        dsBroadcastElement.setUri("https://cdnmirror.web.cern.ch/cdnmirror/webjars/ds-broadcast/4.0-SNAPSHOT/element/ds-broadcast.html");
        dsBroadcastElement.setUri("http://localhost:8080/webjars/ds-broadcast/4.0-SNAPSHOT/element/ds-broadcast.html");
        Element highchartsElement = new Element("webc-highcharts-4.0-SNAPSHOT","webc-highcharts");
//        highchartsElement.setUri("https://cdnmirror.web.cern.ch/cdnmirror/webjars/webc-highcharts/4.0-SNAPSHOT/element/webc-highcharts.html");
        highchartsElement.setUri("http://localhost:8080/webjars/webc-highcharts/4.0-SNAPSHOT/element/webc-highcharts.html");

        Element liveSvgElement = new Element("webc-live-svg-4.0-SNAPSHOT","webc-live-svg");
        //liveSvgElement.setUri("https://cdnmirror.web.cern.ch/cdnmirror/webjars/webc-live-svg/4.0-SNAPSHOT/element/webc-live-svg.html");
        liveSvgElement.setUri("http://localhost:8080/webjars/webc-live-svg/4.0-SNAPSHOT/element/webc-live-svg.html");

        Catalog c = new Catalog("index");
        c.setCategory("general");
        Set<Element> elements = new HashSet<>();
        elements.add(dsBroadcastElement);
        elements.add(highchartsElement);
        elements.add(liveSvgElement);

        c.setElements(elements);
        repository.save(c);


        assert repository.findOne("index").getElements().size() == elements.size();

        elementEntityProcessor.enrichElements();
    }
}