package cern.dash.pjs.process;

import org.springframework.core.io.Resource;

import java.util.concurrent.Future;

/**
 * Created by bcopy on 28/02/17.
 */
public interface PolymerAnalyzerDelegator {
    Future<String> getPolymerElementDescriptor(Resource resource);
}
