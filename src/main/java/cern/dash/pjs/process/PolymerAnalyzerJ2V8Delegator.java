package cern.dash.pjs.process;

import com.eclipsesource.v8.*;
import lombok.SneakyThrows;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.AsyncResult;
import org.springframework.stereotype.Component;

import java.io.*;
import java.util.concurrent.Future;

/**
 * Uses J2V8 to interface with Node and execute JSON extractions using Polymer Analyzer
 * Created by bcopy on 01/03/17.
 */
@Component
public class PolymerAnalyzerJ2V8Delegator implements PolymerAnalyzerDelegator {

    private Resource polymerToJsonScriptResource = new ClassPathResource("polymer-to-json.js"); //"src/main/node/polymer-to-json.js"

    private File polymerToJsonFile;

    public PolymerAnalyzerJ2V8Delegator(Resource polymerToJsonScriptResource) throws IOException {
        this.polymerToJsonScriptResource = polymerToJsonScriptResource;
        validate();
    }


    public PolymerAnalyzerJ2V8Delegator() throws IOException {
        validate();
    }

    private void validate() throws IOException {
        polymerToJsonFile = polymerToJsonScriptResource.getFile();
    }

    @Override
    @Async
    @SneakyThrows
    public Future<String> getPolymerElementDescriptor(Resource elementResource) {

        NodeJS nodeJS = NodeJS.createNodeJS();
        V8Object polymerToJson = nodeJS.require(polymerToJsonFile);


        final V8 runtime = nodeJS.getRuntime();
        runtime.add("polymerToJson", polymerToJson);


        final StringBuffer jsonResult = new StringBuffer();
        final StringBuffer errorResult = new StringBuffer();
        File elementTempFile = convertInputstreamToTempFile("element-", elementResource.getInputStream());
        V8Value filepath = runtime.add("filepath", elementTempFile.getAbsolutePath());
        JavaVoidCallback successCallback = new JavaVoidCallback() {
            @Override
            public void invoke(V8Object v8Object, V8Array v8Array) { jsonResult.append(v8Array.getString(0));  }
        };
        JavaVoidCallback errorCallback = new JavaVoidCallback() {
            @Override
            public void invoke(V8Object v8Object, V8Array v8Array) {
                errorResult.append(v8Array.getString(0));
//                errorResult.append("error");
            }
        };
        V8Object successCb = runtime.registerJavaMethod(successCallback, "onSuccess");
        V8Object errorCb = runtime.registerJavaMethod(errorCallback, "onError");

        runtime.getRuntime().executeObjectScript("polymerToJson.extractJson(filepath).then(onSuccess) ");// .catch(onError)

        while (nodeJS.isRunning()) {
            nodeJS.handleMessage();
        }

        elementTempFile.delete();
//        filepath.release();
//        successCb.release();
//        errorCb.release();
//        polymerToJson.release();
        runtime.release(false);
        if (errorResult.length() != 0) {
            throw new RuntimeException("Node script execution failure : " + errorResult.toString());
        } else {
            return new AsyncResult<>(jsonResult.toString());
        }
    }

    @SneakyThrows
    protected static final File convertInputstreamToTempFile(String prefix, InputStream is) {
        File result = File.createTempFile(prefix, ".html");
        OutputStream outputStream =
                new FileOutputStream(result);

        int read = 0;
        byte[] bytes = new byte[1024];

        while ((read = is.read(bytes)) != -1) {
            outputStream.write(bytes, 0, read);
        }
        outputStream.flush();
        outputStream.close();

        return result;
    }
}
