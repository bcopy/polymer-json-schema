package cern.dash.pjs.process;

import cern.dash.pjs.alpacaschema.dto.PropertyDto;
import cern.dash.pjs.alpacaschema.dto.SchemaDto;
import cern.dash.pjs.polymer.dto.ElementDto;
import cern.dash.pjs.polymer.dto.ElementPropertyDto;
import lombok.extern.java.Log;
import org.springframework.stereotype.Component;

/**
 * Created by bcopy on 02/03/17.
 */
@Component
@Log
public class PolymerToAlpacaMapper {


    public SchemaDto convertPolymerElement(ElementDto element) {
        SchemaDto result = new SchemaDto();
        result.setTitle(element.getTagName());
        for (ElementPropertyDto prop : element.properties) {
            if ( (!prop.getName().startsWith("_"))  && (!"Function".equals(prop.getType()) ) ) {
                PropertyDto pdto = new PropertyDto();
                pdto.setTitle(( prop.getDescription().isEmpty()? prop.getName()  : prop.getDescription()));
                pdto.setType(prop.getType());
                result.getProperties().put(prop.getName(), pdto);
            } else {
                log.finest("Skipping internal property " + prop.getName());
            }
        }
        return result;
    }
}
