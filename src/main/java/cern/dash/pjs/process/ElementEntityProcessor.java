package cern.dash.pjs.process;

import cern.dash.pjs.alpacaschema.dto.SchemaDto;
import cern.dash.pjs.alpacaschema.dto.SchemaWrapperDto;
import cern.dash.pjs.jpa.Element;
import cern.dash.pjs.jpa.ElementRepository;
import cern.dash.pjs.polymer.dto.ElementDto;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.java.Log;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.net.MalformedURLException;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import java.util.logging.Level;

/**
 * Created by bcopy on 02/03/17.
 */
@Component
@Log
public class ElementEntityProcessor {
    @Autowired
    ElementRepository elementRepository;


    @Autowired
    PolymerAnalyzerDelegator analyzer;

    @Autowired
    PolymerToAlpacaMapper schemaMapper;

    public void enrichElements(){
        ObjectMapper mapper = new ObjectMapper();
        mapper.setSerializationInclusion(JsonInclude.Include.NON_EMPTY);
        for(Element elementEntity : elementRepository.findAll()){
            try {
                // Fetch sources locally
                Resource srcResource = new UrlResource(elementEntity.getUri());

                String polymerDescriptor = analyzer.getPolymerElementDescriptor(srcResource).get(10, TimeUnit.SECONDS);

                List<ElementDto> elements= Arrays.asList(mapper.readValue(polymerDescriptor, ElementDto[].class));

                if(! elements.isEmpty()) {
                    ElementDto element = elements.get(elements.size()-1);

                    elementEntity.setTagName(element.getTagName());
                    SchemaDto schemaContents = schemaMapper.convertPolymerElement(element);
                    elementEntity.setSchema(mapper.writeValueAsString(new SchemaWrapperDto(schemaContents)));
                }else{
                    throw new MalformedURLException("Could not find any valid element in "+elementEntity.getUri().toString());
                }

                elementRepository.save(elementEntity);
            }catch(MalformedURLException urle){

                log.log(Level.WARNING, "While getting source file for '"+elementEntity.getId()+"'",urle);
            } catch (InterruptedException|ExecutionException|TimeoutException e) {
                log.log(Level.WARNING, "While running Polymer Analyzer for element "+elementEntity.getId(),e);
                e.printStackTrace();
            } catch (JsonParseException|JsonMappingException e) {
                log.log(Level.WARNING, "While parsing Polymer analyzer element descriptor for '"+elementEntity.getId()+"'",e);
                e.printStackTrace();
            } catch (IOException e) {
                log.log(Level.WARNING, "IO Exception while enriching '"+elementEntity.getId()+"'",e);
                e.printStackTrace();
            }
        }

    }
}
