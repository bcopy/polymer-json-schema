package cern.dash.pjs.jpa;


import lombok.*;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Created by bcopy on 28/02/17.
 */
@Data
@Entity
public class Element implements Serializable {

    @Id
    String id;

    @Column(nullable = false)
    @NonNull
    String tagName;

    String uri;

    String source;

    @Column(length = 4096)
    String schema;

    @java.beans.ConstructorProperties({"id", "tagName"})
    public Element(String id, String tagName) {
        this.id = id;
        this.tagName = tagName;
    }

    public Element() {
    }

    protected boolean canEqual(Object other) {
        return other instanceof Element;
    }

}
