package cern.dash.pjs.jpa;

/**
 * Created by bcopy on 28/02/17.
 */

import java.util.List;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource
public interface ElementRepository extends PagingAndSortingRepository<Element, String> {
}
