package cern.dash.pjs.jpa;

import lombok.*;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Set;

/**
 * Created by bcopy on 28/02/17.
 */
@Data
@Entity
public class Catalog implements Serializable {
    @Id
    String id;

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    private Set<Element> elements;

    private String category;

    @java.beans.ConstructorProperties({"id"})
    public Catalog(String id) {
        this.id = id;
    }

    @java.beans.ConstructorProperties({"id", "elements", "category"})
    public Catalog(String id, Set<Element> elements, String category) {
        this.id = id;
        this.elements = elements;
        this.category = category;
    }

    public Catalog() {
    }

    protected boolean canEqual(Object other) {
        return other instanceof Catalog;
    }

}
