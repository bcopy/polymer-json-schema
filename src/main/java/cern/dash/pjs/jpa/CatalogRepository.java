package cern.dash.pjs.jpa;

/**
 * Created by bcopy on 28/02/17.
 */

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource()
public interface CatalogRepository extends PagingAndSortingRepository<Catalog, String> {
}
