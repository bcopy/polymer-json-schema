package cern.dash.pjs.polymer.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

/**
 * Created by bcopy on 02/03/17.
 */
@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class ParameterDto {
    String name;
    String type;
}
