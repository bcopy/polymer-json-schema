package cern.dash.pjs.polymer.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.util.List;

/**
 * Created by bcopy on 02/03/17.
 */
@lombok.Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class ElementDto {
    public String tagName;
    public String description;
    public List<ElementPropertyDto> properties;
    public List<String> demos;
    public List<AttributeDto> attributes;
//    public List<EventDto> events;


}
