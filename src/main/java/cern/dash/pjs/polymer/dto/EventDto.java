package cern.dash.pjs.polymer.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.util.List;

/**
 * Created by bcopy on 02/03/17.
 */
@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class EventDto {
    String name;
    List<ParameterDto> params;
}
