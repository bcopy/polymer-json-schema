package cern.dash.pjs.polymer.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * Created by bcopy on 02/03/17.
 */
@lombok.Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class AttributeDto {
    String name;
    String type;
    String description;
}
