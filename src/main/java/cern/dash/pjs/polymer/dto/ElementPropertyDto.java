package cern.dash.pjs.polymer.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by bcopy on 02/03/17.
 */
@lombok.Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class ElementPropertyDto {
    String name;
    String type;
    @JsonProperty("default")
    public String defaultValue;
    public String description = "";

}
