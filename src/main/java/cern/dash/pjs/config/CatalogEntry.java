package cern.dash.pjs.config;


import java.net.URI;

/**
 * Created by bcopy on 28/02/17.
 */
@lombok.Data
public class CatalogEntry {
    public URI uri;
    public String localDir;
}
