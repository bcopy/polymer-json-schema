package cern.dash.pjs.config;

import lombok.extern.java.Log;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by bcopy on 28/02/17.
 */
@Component
@ConfigurationProperties(prefix="catalog")
@lombok.Data
@Log
public class PolymerCatalogConfig {
    String id;
    public PolymerCatalogConfig(){
      log.warning("************ Instantiated");
    }
    public List<CatalogEntry> entries = new ArrayList<>();

}
