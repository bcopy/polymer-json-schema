package cern.dash.pjs;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PolymerJsonSchemaApplication {

	public static void main(String[] args) {
		SpringApplication.run(PolymerJsonSchemaApplication.class, args);
	}
}
